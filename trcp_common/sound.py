#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from trcp_common.trcpSound import song

# メイン
def main(file):
    # trcp_common/src/trcp_common/data/sound/0326.wav
    song(file)

if __name__ == '__main__':
    args = sys.argv
    main(args[1])

# rosrun trcp_common sound.py src/trcp/trcp_common/data/sound/0326.wav 
    
